#include <stdlib.h>
#include <stdio.h>
#include <jpeglib.h>

#include <iostream>
#include <fstream>

#include "line_analyze.h"
#include "paint_line.h"

using namespace std;

int main(int argc, const char** argv)
{
	if(argc<4)
	{
		cerr << "Usage: <file_input> <file_output> <file_datapoints>" << endl;
		return 1;
	}

	// file vars
	FILE* input;
	FILE* output;
	ofstream datapoints (argv[3]);

	// line vars
	JDIMENSION curline;
	JSAMPARRAY line;
	int        line_length;

	// analysis vars
	int avg;
	int begin;
	int end;

	// jpeg decompress vars
	struct jpeg_decompress_struct jdinfo;
	struct jpeg_error_mgr         jderr;

	// jpeg compress vars
	struct jpeg_compress_struct jcinfo;
	struct jpeg_error_mgr       jcerr;

	// initialize jpeg decompress struct
	jdinfo.err=jpeg_std_error(&jderr);
	jpeg_create_decompress(&jdinfo);

	// initialize jpeg compress struct
	jcinfo.err=jpeg_std_error(&jcerr);
	jpeg_create_compress(&jcinfo);

	// open input file
	input=fopen(argv[1], "rb");
	if(!input)
	{
		cerr << "Error opening file." << endl;
		return 1;
	}

	// decompress file and load headers
	jpeg_stdio_src(&jdinfo, input);
	jpeg_read_header(&jdinfo, TRUE);
	jpeg_start_decompress(&jdinfo);

	// open output file
	output=fopen(argv[2], "wb");
	if(!output)
	{
		cerr << "Could not write to file." << endl;
		return 1;
	}

	// setup compression stuff
	jpeg_stdio_dest(&jcinfo, output);
	jcinfo.image_width=jdinfo.image_width;
	jcinfo.image_height=jdinfo.image_height;
	jcinfo.input_components=jdinfo.output_components;
	jcinfo.in_color_space=JCS_RGB;
	jpeg_set_defaults(&jcinfo);
	jpeg_set_quality(&jcinfo, 80, TRUE);
	jpeg_start_compress(&jcinfo, TRUE);

	// allocate space for a line
	line_length=jdinfo.output_width*jdinfo.output_components;
	line=(*jdinfo.mem->alloc_sarray)(
		(j_common_ptr) &jdinfo, JPOOL_IMAGE, line_length, 1
	);

	// debug crap
	cout << "HEIGHT: " << jdinfo.output_height << endl;
	cout << "WIDTH: " << jdinfo.output_width << endl;

	for(curline=0; curline<jdinfo.output_height; curline++)
	{
		// read scanline
		jpeg_read_scanlines(&jdinfo, line, 1);

		// analyze and paint
		if(line_analyze(&jdinfo, line, begin, end))
		{
			paint_line(&jdinfo, line, begin, end);

			// output datapoints to file
			avg=(begin+end)/2;
			datapoints << avg << " " << curline << endl;
		}

		// write scanlines
		jpeg_write_scanlines(&jcinfo, line, 1);
	}

	// done
	jpeg_finish_decompress(&jdinfo);
	jpeg_destroy_decompress(&jdinfo);
	jpeg_finish_compress(&jcinfo);
	jpeg_destroy_compress(&jcinfo);
	fclose(input);
	fclose(output);

	// TODO: free line variable

	// done
	cout << "done" << endl;
	return 0;
}

