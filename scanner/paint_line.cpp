#include "paint_line.h"

#define FILL_COLOR_R 0
#define FILL_COLOR_G 255
#define FILL_COLOR_B 0

#define AVG_COLOR_R 0
#define AVG_COLOR_G 0
#define AVG_COLOR_B 0

#define AVG_LINE_WIDTH 4

void paint_line(
	struct jpeg_decompress_struct* jdinfo,
	JSAMPARRAY line,
	int begin,
	int end
){
	int avg;
	int i;

	// fill line
	for(i=begin; i<end; i++)
	{
		line[0][i*jdinfo->output_components+0]=FILL_COLOR_R;
		line[0][i*jdinfo->output_components+1]=FILL_COLOR_G;
		line[0][i*jdinfo->output_components+2]=FILL_COLOR_B;
	}

	// paint avg line
	avg=(begin+end)/2;
	for(i=0; i<AVG_LINE_WIDTH; i++)
	{
		line[0][(i+avg)*jdinfo->output_components+0]=AVG_COLOR_R;
		line[0][(i+avg)*jdinfo->output_components+1]=AVG_COLOR_G;
		line[0][(i+avg)*jdinfo->output_components+2]=AVG_COLOR_B;
	}
}
