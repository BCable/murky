#include <stdlib.h>
#include <stdio.h>
#include <jpeglib.h>

#include <iostream>

bool line_analyze(
	struct jpeg_decompress_struct* jdinfo,
	JSAMPARRAY line,
	int& begin,
	int& end
);
