#include "line_analyze.h"

#define FUDGE_FACTOR_RANGE_R 50
#define FUDGE_FACTOR_RANGE_G 50
#define FUDGE_FACTOR_RANGE_B 50

// red in water
/*
#define TARGET_R 182
#define TARGET_G 61
#define TARGET_B 73
//*/
// red on plate
/*
#define TARGET_R 120
#define TARGET_G 20
#define TARGET_B 20
//*/
// black
/*
#define TARGET_R 20
#define TARGET_G 20
#define TARGET_B 20
//*/
// white
//*
#define TARGET_R 225
#define TARGET_G 225
#define TARGET_B 225
//*/

using namespace std;

bool line_analyze(
	struct jpeg_decompress_struct* jdinfo,
	JSAMPARRAY line,
	int& begin,
	int& end
){
	unsigned int i;

	// rgb vars
	int  r;
	int  g;
	int  b;

	// working vars
	int  widest_begin;
	int  widest_end;
	int  working_begin;
	int  working_end;

	// set up variables
	widest_begin=-1;
	widest_end=-1;
	working_begin=-1;
	working_end=-1;

	// finds the widest area that matches the fudge factor range
	for(i=0; i<jdinfo->output_width; i++)
	{
		r=line[0][i*jdinfo->output_components+0];
		g=line[0][i*jdinfo->output_components+1];
		b=line[0][i*jdinfo->output_components+2];

		if(
			// red
			(TARGET_R+FUDGE_FACTOR_RANGE_R>r) &&
			(TARGET_R-FUDGE_FACTOR_RANGE_R<r) &&

			// green
			(TARGET_G+FUDGE_FACTOR_RANGE_G>g) &&
			(TARGET_G-FUDGE_FACTOR_RANGE_G<g) &&

			// blue
			(TARGET_B+FUDGE_FACTOR_RANGE_B>b) &&
			(TARGET_B-FUDGE_FACTOR_RANGE_B<b)
		){

			// if we're starting a new detected section
			if(working_begin==-1)
			{
				working_begin=i;
			}
			else
			{
				working_end=i;

				// if this instance is wider than the previously found ones
				if(widest_end-widest_begin<working_end-working_begin)
				{
					widest_begin=working_begin;
					widest_end=working_end;
				}
			}
		}

		// clear out working variables
		else if(working_begin!=-1)
		{
			working_begin=-1;
			working_end=-1;
		}
	}

	begin=widest_begin;
	end=widest_end;

	// debug
	//cout << "begin:" << begin << endl;
	//cout << "end:" << end << endl;

	if(begin==-1)
		return false;

	else if(end==-1)
		end=begin;

	return true;
}
