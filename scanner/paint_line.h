#include <stdlib.h>
#include <stdio.h>
#include <jpeglib.h>

void paint_line(
	struct jpeg_decompress_struct* jdinfo,
	JSAMPARRAY line,
	int begin,
	int end
);
