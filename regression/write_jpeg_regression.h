#include <stdio.h>
#include <jpeglib.h>

#include <iostream>

#include "paint_line.h"

void write_jpeg_regression(
	FILE* input,
	FILE* output,
	double slope,
	double yintercept,
	int min_y,
	int max_y
);
