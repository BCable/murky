#include <iostream>
#include <vector>

#include "calculate_linear_regression.h"
#include "format_datapoints.h"
#include "point_struct.h"
#include "write_jpeg_regression.h"

using namespace std;

int main(int argc, char** argv)
{
	if(argc<4)
	{
		cerr << "Usage: <file_datapoints> <file_input> <file_output>" << endl;
		return 1;
	}

	// file vars
	ifstream datapoints (argv[1]);
	vector<struct point> points;
	FILE* input;
	FILE* output;

	// regression vars
	double slope;
	double yintercept;

	// reformat datapoints
	format_datapoints(datapoints, points);

	// analyze datapoints
	calculate_linear_regression(points, slope, yintercept);

	// open input file
	input=fopen(argv[2], "rb");
	printf("%s\n", argv[2]);
	if(!input)
	{
		cerr << "Error opening file." << endl;
		return 1;
	}

	// open output file
	output=fopen(argv[3], "wb");
	if(!output)
	{
		cerr << "Could not write to file." << endl;
		return 1;
	}

	// print regression to screen
	cout << "y = " << slope << "x + " << yintercept << endl;

	// start jpeg decompression
	write_jpeg_regression(
		input, output,
		slope, yintercept,
		points[0].y, points[points.size()-1].y
	);

	// done
	cout << "done" << endl;
	return 0;
}
