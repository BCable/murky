#include <vector>
#include <fstream>

#include "point_struct.h"

void format_datapoints(
	std::ifstream& datapoints,
	std::vector<struct point>& points
);
