#include "paint_line.h"

#define REG_LINE_R 255
#define REG_LINE_G 0
#define REG_LINE_B 0

#define REG_EXTRAP_LINE_R 0
#define REG_EXTRAP_LINE_G 255
#define REG_EXTRAP_LINE_B 0

#define FILL_LINE_WIDTH 2

void paint_line(
	struct jpeg_decompress_struct* jdinfo,
	int line_y,
	JSAMPARRAY line,
	double slope,
	double yintercept,
	int min_y,
	int max_y
){
	unsigned int i;
	int x;

	x=(line_y-yintercept)/slope;

	for(i=0; i<jdinfo->output_width; i++)
	{
		// fill line
		if(
			(int) i>=x-(FILL_LINE_WIDTH/2) &&
			(int) i<=x+(FILL_LINE_WIDTH/2)
		){
			if(line_y>=min_y && line_y<=max_y)
			{
				line[0][i*jdinfo->output_components+0]=REG_LINE_R;
				line[0][i*jdinfo->output_components+1]=REG_LINE_G;
				line[0][i*jdinfo->output_components+2]=REG_LINE_B;
			}
			else
			{
				line[0][i*jdinfo->output_components+0]=REG_EXTRAP_LINE_R;
				line[0][i*jdinfo->output_components+1]=REG_EXTRAP_LINE_G;
				line[0][i*jdinfo->output_components+2]=REG_EXTRAP_LINE_B;
			}
		}
	}
}
