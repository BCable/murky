#include "calculate_linear_regression.h"

using namespace std;

void calculate_linear_regression(
	vector<struct point> points,
	double& slope,
	double& yintercept
){
	// averages
	double avg_x;
	double avg_y;

	// final division
	double numerator;
	double denominator;

	vector<struct point>::iterator it;

	// calculate avg x and y values
	for(it=points.begin(); it!=points.end(); it++)
	{
		avg_x+=(*it).x;
		avg_y+=(*it).y;
	}

	avg_x/=points.size();
	avg_y/=points.size();

	// sum x and y residuals and make the final formula
	for(it=points.begin(); it!=points.end(); it++)
	{
		numerator+=((*it).x-avg_x)*((*it).y-avg_y);
		denominator+=((*it).x-avg_x)*((*it).x-avg_x);
	}

	// finish slope
	slope=numerator/denominator;

	// calculate y-intercept
	yintercept=avg_y-(slope*avg_x);
}
