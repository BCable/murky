#include <vector>

#include "point_struct.h"

void calculate_linear_regression(
	std::vector<struct point>,
	double& slope,
	double& yintercept
);
