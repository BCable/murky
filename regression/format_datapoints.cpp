#include "format_datapoints.h"

#include <iostream>

using namespace std;

void format_datapoints(ifstream& datapoints, vector<struct point>& points)
{
	struct point tmp;

	while(!datapoints.eof())
	{
		datapoints >> tmp.x;
		datapoints >> tmp.y;

		points.push_back(tmp);
	}
}
