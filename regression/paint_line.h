#include <stdio.h>
#include <jpeglib.h>

void paint_line(
	struct jpeg_decompress_struct* jdinfo,
	int line_y,
	JSAMPARRAY line,
	double slope,
	double yintercept,
	int min_y,
	int max_y
);
