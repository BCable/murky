#include "write_jpeg_regression.h"

using namespace std;

void write_jpeg_regression(
	FILE* input,
	FILE* output,
	double slope,
	double yintercept,
	int min_y,
	int max_y
){
	// line vars
	JDIMENSION curline;
	JSAMPARRAY line;
	int        line_length;

	// jpeg decompress vars
	struct jpeg_decompress_struct jdinfo;
	struct jpeg_error_mgr         jderr;

	// jpeg compress vars
	struct jpeg_compress_struct jcinfo;
	struct jpeg_error_mgr       jcerr;

	// initialize jpeg decompress struct
	jdinfo.err=jpeg_std_error(&jderr);
	jpeg_create_decompress(&jdinfo);

	// initialize jpeg compress struct
	jcinfo.err=jpeg_std_error(&jcerr);
	jpeg_create_compress(&jcinfo);

	// decompress file and load headers
	jpeg_stdio_src(&jdinfo, input);
	jpeg_read_header(&jdinfo, TRUE);
	jpeg_start_decompress(&jdinfo);

	// setup compression stuff
	jpeg_stdio_dest(&jcinfo, output);
	jcinfo.image_width=jdinfo.image_width;
	jcinfo.image_height=jdinfo.image_height;
	jcinfo.input_components=jdinfo.output_components;
	jcinfo.in_color_space=JCS_RGB;
	jpeg_set_defaults(&jcinfo);
	jpeg_set_quality(&jcinfo, 80, TRUE);
	jpeg_start_compress(&jcinfo, TRUE);

	// allocate space for a line
	line_length=jdinfo.output_width*jdinfo.output_components;
	line=(*jdinfo.mem->alloc_sarray)(
		(j_common_ptr) &jdinfo, JPOOL_IMAGE, line_length, 1
	);

	// debug crap
	cout << "HEIGHT: " << jdinfo.output_height << endl;
	cout << "WIDTH: " << jdinfo.output_width << endl;

	for(curline=0; curline<jdinfo.output_height; curline++)
	{
		// read scanline
		jpeg_read_scanlines(&jdinfo, line, 1);

		// paint line
		paint_line(&jdinfo, curline, line, slope, yintercept, min_y, max_y);

		// write scanline
		jpeg_write_scanlines(&jcinfo, line, 1);
	}

	// done
	jpeg_finish_decompress(&jdinfo);
	jpeg_destroy_decompress(&jdinfo);
	jpeg_finish_compress(&jcinfo);
	jpeg_destroy_compress(&jcinfo);
	fclose(input);
	fclose(output);

	// TODO: free line variable

}
